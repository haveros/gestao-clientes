﻿using GestaoCliente.Domain.Clientes;
using Microsoft.EntityFrameworkCore;
using ClienteAgg = GestaoCliente.Domain.Clientes;

namespace GestaoCliente.Infra.Data.Client
{
    public class ClientRepository : IClientRepository
    {

        private readonly EntityDbContext context;

        public ClientRepository(EntityDbContext context)
        {
            this.context = context;
        }
        public async Task<ClienteAgg.Client?> GetAsync(Guid id)
        {
            return await context.Set<ClienteAgg.Client>().FirstOrDefaultAsync(c=> c.Id == id);
        }

        public async Task AddAsync(ClienteAgg.Client entity)
        {
            await context.Set<ClienteAgg.Client>().AddAsync(entity);
        }

        public async Task<List<ClienteAgg.Client>> GetAllAsync()
        {
            return await context.Set<ClienteAgg.Client>().ToListAsync();
        }

        public void Update(ClienteAgg.Client entity)
        {
            context.Set<ClienteAgg.Client>().Update(entity);
        }

        public void Delete(ClienteAgg.Client entity)
        {
            context.Set<ClienteAgg.Client>().Remove(entity);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GestaoCliente.Infra.Data.Client
{
    public class ClientConfiguration : IEntityTypeConfiguration<Domain.Clientes.Client>
    {
        public void Configure(EntityTypeBuilder<Domain.Clientes.Client> builder)
        {
            builder.ToTable("Cliente");
        }
    }
}

﻿using GestaoCliente.Domain.Core;
using GestaoCliente.Domain.Core.Model;
using GestaoCliente.Infra.Data.Client;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace GestaoCliente.Infra.Data
{
    public class EntityDbContext : DbContext
    {
        public EntityDbContext(DbContextOptions options) : base(options) { }
        public DbSet<Domain.Clientes.Client> Clientes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Ignore<Event>();

            modelBuilder.ApplyConfiguration(new ClientConfiguration());
        }

        public override int SaveChanges()
        {
            var entries = ChangeTracker
                .Entries()
                .Where(e => e.Entity is BaseEntity && (
                        e.State == EntityState.Added
                        || e.State == EntityState.Modified));

            foreach (var entityEntry in entries)
            {
                if (entityEntry.State == EntityState.Added)
                {
                    ((BaseEntity)entityEntry.Entity).DataCadastro = DateTime.Now;
                }
            }

            return base.SaveChanges();
        }
    }
}

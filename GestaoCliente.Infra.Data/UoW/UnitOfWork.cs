﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoCliente.Infra.Data.UoW
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly EntityDbContext sistemaDeComprasContexto;

        public UnitOfWork(EntityDbContext context)
        {
            sistemaDeComprasContexto = context;
        }

        public bool Commit()
        {
            return sistemaDeComprasContexto.SaveChanges() > 0;
        }

        public void Dispose()
        {
            sistemaDeComprasContexto.Dispose();
        }
    }
}

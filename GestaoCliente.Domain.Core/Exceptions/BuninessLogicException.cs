﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoCliente.Domain.Core.Exceptions
{
    public class BuninessLogicException : Exception
    {
        public BuninessLogicException(string message) : base(message) { }
    }
}

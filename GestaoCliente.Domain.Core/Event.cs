﻿using MediatR;

namespace GestaoCliente.Domain.Core
{
    public abstract class Event : INotification
    {
        public DateTime DataOcorrencia => DateTime.Now;
    }
}

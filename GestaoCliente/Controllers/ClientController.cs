﻿using GestaoCliente.Api.Models;
using GestaoCliente.Application.Cliente.Command.AddClient;
using GestaoCliente.Application.Cliente.Command.DeleteClient;
using GestaoCliente.Application.Cliente.Command.UpdateClient;
using GestaoCliente.Application.Cliente.Query;
using GestaoCliente.Domain.Clientes;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace GestaoCliente.Api.Controllers
{
    public class ClienteController : Controller
    {
        private readonly IMediator mediator;

        public ClienteController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        /// <summary>
        /// lista todos os clientes
        /// </summary>
        /// <returns></returns>
        [HttpGet("clientes")]
        [ProducesResponseType(typeof(ReturnModelView<List<GetClientViewModel>>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ReturnModelView<List<ErrorModelView>>), (int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> ListAll()
        {
            var query = new GetAllClientsQuery();
            var clients = await mediator.Send(query);

            var clientReturn = new ReturnModelView<List<GetClientViewModel>>()
            {
                ObjectReturn = clients
            };

            return Ok(clientReturn);
        }

        /// <summary>
        /// busca um cliente pelo ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("clientes/{id}")]
        [ProducesResponseType(typeof(ReturnModelView<GetClientViewModel>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ReturnModelView<ErrorModelView>), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ReturnModelView<ErrorModelView>), (int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> GetById(Guid id)
        {
            var query = new GetClientQuery() { Id = id };
            var client = await mediator.Send(query);

            var clientReturn = new ReturnModelView<GetClientViewModel>()
            {
                ObjectReturn = client
            };

            return Ok(clientReturn);
        }

        /// <summary>
        /// Cadastra um novo cliente
        /// </summary>
        /// <param name="addClientCommand"></param>
        /// <returns></returns>
        [HttpPost("clientes")]
        [ProducesResponseType(typeof(ReturnModelView<AddClientModelView>), (int)HttpStatusCode.Created)]
        [ProducesResponseType(typeof(ReturnModelView<ErrorModelView>), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(ReturnModelView<ErrorModelView>), (int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> CadastrarCliente([FromBody] AddClientCommand addClientCommand)
        {
            var client = await mediator.Send(addClientCommand);
            var clientReturn = new ReturnModelView<AddClientModelView>()
            {
                ObjectReturn = client
            };

            return CreatedAtRoute(client.Id, clientReturn);
        }

        /// <summary>
        /// atualiza um cliente existente
        /// </summary>
        /// <param name="updateClientCommand"></param>
        /// <returns></returns>
        [HttpPut("clientes/{id}")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        [ProducesResponseType(typeof(ReturnModelView<ErrorModelView>), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ReturnModelView<ErrorModelView>), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(ReturnModelView<ErrorModelView>), (int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> UpdateClient(Guid id, [FromBody] UpdateClientCommand updateClientCommand)
        {
            updateClientCommand.IdCliente = id;

            await mediator.Send(updateClientCommand);

            return NoContent();
        }

        /// <summary>
        /// deleta um cliente pelo ID
        /// </summary>
        /// <param name="deleteClientCommand"></param>
        /// <returns></returns>
        [HttpDelete("cliente/{id}")]
        [ProducesResponseType(typeof(ReturnModelView<ErrorModelView>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ReturnModelView<ErrorModelView>), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ReturnModelView<ErrorModelView>), (int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> DeleteClient(Guid id)
        {
            var deleteClientCommand = new DeleteClientCommand() { Id = id };

            var client = await mediator.Send(deleteClientCommand);

            return Ok(client);
        }
    }
}

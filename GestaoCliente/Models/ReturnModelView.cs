﻿using System.Text.Json.Serialization;

namespace GestaoCliente.Api.Models
{
    /// <summary>
    /// Retorno padrao das controllers
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ReturnModelView<T> where T : class, new()
    {
        [JsonPropertyName("sucesso")]
        public bool Success { get; set; } = true;
        [JsonPropertyName("mensagem")]
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public string Message { get; set; }
        [JsonPropertyName("ObjetoRetorno")]
        public T ObjectReturn { get; set; } = new T();
    }
}

﻿namespace GestaoCliente.Api.Models
{
    public class ErrorModelView
    {
        public string Controller { get; set; }
        /// <summary>
        /// Action
        /// </summary>
        public string Action { get; set; }
    }
}

﻿using Azure.Core;
using GestaoCliente.Api.Models;
using GestaoCliente.Application.Cliente.Query;
using GestaoCliente.Domain.Clientes;
using GestaoCliente.Domain.Core.Exceptions;
using MediatR;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net;
using System.Text;

namespace GestaoCliente.Api.Middlewares
{
    public class ExceptionHandlingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<ExceptionHandlingMiddleware> _logger;
        public ExceptionHandlingMiddleware(RequestDelegate next, ILogger<ExceptionHandlingMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _next.Invoke(httpContext);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(httpContext, ex);
            }
        }
        private async Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            var response = context.Response;
            string controller = context.GetRouteData().Values["controller"]?.ToString() ?? "";
            string action = context.GetRouteData().Values["action"]?.ToString() ?? "";

            var errorResponse = new ReturnModelView<ErrorModelView>()
            {
                Success = false,
                Message = exception.Message,
                ObjectReturn = new()
                {
                    Controller = controller,
                    Action = action,
                }
            };

            context.Response.ContentType = "application/json";

            switch (exception)
            {
                case KeyNotFoundException:
                case NotFoundException:
                    response.StatusCode = (int)HttpStatusCode.NotFound;
                    break;
                case BuninessLogicException:
                    response.StatusCode = (int)HttpStatusCode.BadRequest; 
                    break;
                default:
                    response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    errorResponse.Message = "Internal Server errors. Check Logs!";
                    break;
            }
            _logger.LogError(exception.Message);
            var result = JsonConvert.SerializeObject(errorResponse);
            await context.Response.WriteAsync(result);
        }


    }
}

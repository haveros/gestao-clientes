﻿using GestaoCliente.Domain.Clientes;
using GestaoCliente.Infra.Data;
using GestaoCliente.Infra.Data.Client;
using GestaoCliente.Infra.Data.UoW;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace GestaoCliente.Api.Configuration
{
    public static class ApiConfiguration
    {
        public static void ConfigureServices(this IServiceCollection services, IConfiguration configuration)
        {

            #region Services
            services.AddScoped<IClientRepository, ClientRepository>();
            #endregion

            #region Configurations
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            #endregion

            services.AddDbContext<EntityDbContext>(options =>
               options.UseSqlServer(
                   configuration.GetConnectionString("DefaultConnection"),
                   o => o.MigrationsAssembly("GestaoCliente.Api"))
           );

            var assembly = AppDomain.CurrentDomain.Load("GestaoCliente.Application");
            services.AddMediatR(cfg => cfg.RegisterServicesFromAssembly(assembly));
            services.AddAutoMapper(assembly);
        }
    }
}

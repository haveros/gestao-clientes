﻿using AutoMapper;
using GestaoCliente.Application.Cliente.Command.DeleteClient;
using GestaoCliente.Application.Cliente.Query;
using GestaoCliente.Domain.Clientes;
using GestaoCliente.Domain.Core.Exceptions;
using GestaoCliente.Infra.Data.UoW;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoCliente.Application.Cliente.Command.DeleteClient
{
    public class DeleteClientCommandHandler : CommandHandler, IRequestHandler<DeleteClientCommand, GetClientViewModel>
    {
        private readonly IClientRepository clienteRepository;
        private readonly IMapper mapper;

        public DeleteClientCommandHandler(IClientRepository clienteRepository, IUnitOfWork uow, IMediator mediator, IMapper mapper) : base(uow, mediator)
        {
            this.clienteRepository = clienteRepository;
            this.mapper = mapper;
        }
        public async Task<GetClientViewModel> Handle(DeleteClientCommand request, CancellationToken cancellationToken)
        {
            var client = await clienteRepository.GetAsync(request.Id);

            if (client == null)
                throw new NotFoundException($"Cliente {request.Id} não encontrado");

            clienteRepository.Delete(client);

            Commit();
            PublishEvents(client.Events);

            return mapper.Map<GetClientViewModel>(client);
        }
    }
}

﻿using GestaoCliente.Application.Cliente.Command.AddClient;
using GestaoCliente.Application.Cliente.Query;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoCliente.Application.Cliente.Command.DeleteClient
{
    public class DeleteClientCommand : IRequest<GetClientViewModel>
    {
        public Guid Id { get; set; }
    }
}

﻿using MediatR;
using System.Text.Json.Serialization;

namespace GestaoCliente.Application.Cliente.Command.UpdateClient
{
    public class UpdateClientCommand : IRequest<bool>
    {
        [JsonIgnore]
        public Guid IdCliente { get; set; }
        [JsonPropertyName("nome")]
        public string Name { get; set; }
        [JsonPropertyName("email")]
        public string Email { get; set; }
        [JsonPropertyName("dataNascimento")]
        public DateTime BirthDate { get; set; }
    }
}

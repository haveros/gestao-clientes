﻿using GestaoCliente.Domain.Clientes;
using GestaoCliente.Domain.Core.Exceptions;
using GestaoCliente.Infra.Data.UoW;
using MediatR;
using ClientAgg = GestaoCliente.Domain.Clientes;

namespace GestaoCliente.Application.Cliente.Command.UpdateClient
{
    public class UpdateClientCommandHandler : CommandHandler, IRequestHandler<UpdateClientCommand, bool>
    {
        private readonly IClientRepository _ClienteRepository;

        public UpdateClientCommandHandler(ClientAgg.IClientRepository ClienteRepository, IUnitOfWork uow, IMediator mediator) : base(uow, mediator)
        {
            this._ClienteRepository = ClienteRepository;
        }
        public async Task<bool> Handle(UpdateClientCommand request, CancellationToken cancellationToken)
        {
            var client = await _ClienteRepository.GetAsync(request.IdCliente);
            if (client == null)
            {
                throw new NotFoundException($"cliente {request.IdCliente} não encontrado");
            }

            client.UpdateClientData(request.Name, request.Email, request.BirthDate);

            _ClienteRepository.Update(client);

            Commit();
            PublishEvents(client.Events);

            return true;
        }
    }
}

﻿using MediatR;

namespace GestaoCliente.Application.Cliente.Command.AddClient
{
    public class AddClientCommand : IRequest<AddClientModelView>
    {
        public string Nome { get; set; }
        public string Email { get; set; }
        public DateTime DataNascimento { get; set; }
    }
}

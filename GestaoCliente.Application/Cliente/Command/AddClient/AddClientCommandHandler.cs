﻿
using AutoMapper;
using GestaoCliente.Domain.Clientes;
using GestaoCliente.Infra.Data.UoW;
using MediatR;
using ClientAgg = GestaoCliente.Domain.Clientes;

namespace GestaoCliente.Application.Cliente.Command.AddClient
{
    public class AddClientCommandHandler : CommandHandler, IRequestHandler<AddClientCommand, AddClientModelView>
    {
        private readonly IClientRepository clienteRepository;
        private readonly IMapper mapper;

        public AddClientCommandHandler(IClientRepository clienteRepository, IUnitOfWork uow, IMediator mediator, IMapper mapper) : base(uow, mediator)
        {
            this.clienteRepository = clienteRepository;
            this.mapper = mapper;
        }
        public async Task<AddClientModelView> Handle(AddClientCommand request, CancellationToken cancellationToken)
        {
            var client = new ClientAgg.Client(request.Nome, request.Email, request.DataNascimento);

            await clienteRepository.AddAsync(client);

            Commit();
            PublishEvents(client.Events);

            return mapper.Map<AddClientModelView>(client);
        }
    }
}

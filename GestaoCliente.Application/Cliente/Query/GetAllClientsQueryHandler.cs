﻿using AutoMapper;
using GestaoCliente.Domain.Clientes;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoCliente.Application.Cliente.Query
{
    public class GetAllClientsQuery : IRequest<List<GetClientViewModel>>
    {

    }

    public class GetAllClientsQueryHandler : IRequestHandler<GetAllClientsQuery, List<GetClientViewModel>>
    {
        private readonly IClientRepository ClienteRepository;
        private readonly IMapper mapper;


        public GetAllClientsQueryHandler(IClientRepository ClienteRepository, IMapper mapper)
        {
            this.ClienteRepository = ClienteRepository;
            this.mapper = mapper;
        }

        public async Task<List<GetClientViewModel>> Handle(GetAllClientsQuery request, CancellationToken cancellationToken)
        {
            var clients = await ClienteRepository.GetAllAsync();
            var clienteViewModel = mapper.Map<IEnumerable<GetClientViewModel>>(clients);

            return clienteViewModel.ToList();
        }
    }
}

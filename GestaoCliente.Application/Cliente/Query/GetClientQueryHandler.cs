﻿using AutoMapper;
using GestaoCliente.Domain.Clientes;
using GestaoCliente.Domain.Core.Exceptions;
using MediatR;

namespace GestaoCliente.Application.Cliente.Query
{
    public class GetClientQueryHandler : IRequestHandler<GetClientQuery, GetClientViewModel>
    {
        private readonly IClientRepository ClienteRepository;
        private readonly IMapper mapper;

        public GetClientQueryHandler(IClientRepository ClienteRepository, IMapper mapper)
        {
            this.ClienteRepository = ClienteRepository;
            this.mapper = mapper;
        }
        public async Task<GetClientViewModel> Handle(GetClientQuery request, CancellationToken cancellationToken)
        {
            var Cliente = await ClienteRepository.GetAsync(request.Id);

            if (Cliente == null)
                throw new NotFoundException($"cliente {request.Id} não encontrado!");

            var ClienteViewModel = mapper.Map<GetClientViewModel>(Cliente);

            return ClienteViewModel;
        }
    }
}

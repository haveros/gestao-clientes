﻿using MediatR;

namespace GestaoCliente.Application.Cliente.Query
{
    public class GetClientQuery : IRequest<GetClientViewModel>
    {
        public Guid Id { get; set; }
    }
}

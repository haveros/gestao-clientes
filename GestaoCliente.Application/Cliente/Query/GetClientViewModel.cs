﻿using System.Text.Json.Serialization;

namespace GestaoCliente.Application.Cliente.Query
{
    public class GetClientViewModel
    {
        /// <summary>
        /// Identificacao unica do cliente
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// Nome do cliente
        /// </summary>
        [JsonPropertyName("nome")]
        public string Name { get; set; }
        /// <summary>
        /// e-mail
        /// </summary>
        [JsonPropertyName("email")]
        public string Email { get; set; }
        /// <summary>
        /// data de nascimento
        /// </summary>
        [JsonPropertyName("dataNascimento")]
        public DateTime BirthDate { get; set; }
    }
}

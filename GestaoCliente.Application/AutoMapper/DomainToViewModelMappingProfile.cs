﻿using AutoMapper;
using GestaoCliente.Application.Cliente.Command.AddClient;
using GestaoCliente.Application.Cliente.Query;
using ClienteAgg = GestaoCliente.Domain.Clientes;

namespace GestaoCliente.Application.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
            CreateMap<ClienteAgg.Client, AddClientModelView>();
            CreateMap<ClienteAgg.Client, GetClientViewModel>();
        }
    }
}

﻿using GestaoCliente.Domain.Core.Exceptions;
using GestaoCliente.Domain.Core.Model;
using System.ComponentModel.DataAnnotations.Schema;

namespace GestaoCliente.Domain.Clientes
{
    public class Client : BaseEntity
    {
        [Column("Nome")]
        public string Name { get; set; }
        [Column("Email")]
        public string Email { get; set; }
        [Column("DataNascimento")]
        public DateTime BirthDate { get; set; }

        public Client()
        {

        }

        public Client(string name, string email, DateTime birthDate)
        {
            Name = name;
            Email = email;
            BirthDate = birthDate;

            ValidateClientData();
        }

        public void UpdateClientData(string name, string email, DateTime birthDate)
        {
            Name = name;
            Email = email;
            BirthDate = birthDate;

            ValidateClientData();
        }

        private void ValidateClientData()
        {
            if (BirthDate > DateTime.Now)
                throw new BuninessLogicException("A data de nascimento não pode ser superior a data atual.");

            if (!ValidateEmail(Email))
            {
                throw new BuninessLogicException("E-mail inválido");
            }
        }
    }
}
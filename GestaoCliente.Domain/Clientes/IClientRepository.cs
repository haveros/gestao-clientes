﻿namespace GestaoCliente.Domain.Clientes
{
    public interface IClientRepository
    {
        Task<Client?> GetAsync(Guid id);
        Task<List<Client>> GetAllAsync();
        Task AddAsync(Client entity);
        void Delete(Client entity);
        void Update(Client entity);
    }
}

# Título do projeto

Exercício técnico de desenvolvimento para gestão de clientes

## 📄 Descrição
O Objetivo deste projeto é seguir com a evolução dos exemplos de código abaixo que não aplicação boas práticas de mercado:

`

    using System;
    namespace MyApp.Controllers
    {
        public class Cliente : ApiControllerAttribute
        {
            private readonly ApplicationDbContext dbContext;

            public Cliente()
            {
                dbContext = new ApplicationDbContext();
            }

            [HttpGet]
            public IEnumerable<Cliente> Clientes()
            {
                return dbContext.Clientes.ToList();
            }

            [HttpGet]
            public IHttpActionResult Clienteid(int id)
            {
                var c1 = dbContext.Clientes.FirstOrDefault(c1 => c1.Id == id);
                if (c1 == null)
                    return NotFound();

                return Ok(c1);
            }
            [HttpPost]

            public IHttpActionResult pegacliente([FromBody] Cliente cliente)
            {
                dbContext.Clientes.Add(cliente);
                dbContext.SaveChanges();

                return CreatedAtRoute("DefaultApi", new { id = cliente.Id }, cliente);
            }

            [HttpPut]
            public IHttpActionResult pegacliente(int id, [FromBody] Cliente cliente)
            {
                if (id != cliente.Id)
                    return NotFound();

                var existingCliente = dbContext.Clientes.FirstOrDefault(c = c => c.Id == id);
                if (existingCliente == null)
                    return NotFound();

                existingCliente.Nome = cliente.Nome;
                existingCliente.Email = cliente.Email;
                existingCliente.DataNascimento = cliente.DataNascimento;

                dbContext.SaveChanges();

                return StatusCode(HttpStatusCode.NoContent);
            }

            [HttpDelete]
            public IHttpActionResult delete(int id)
            {
                var cliente = dbContext.Clientes.FirstOrDefault(c => c.Id == id);
                if (cliente == null)
                    return NotFound();


                dbContext.Clientes.Remove(cliente);
                dbContext.SaveChanges();
                return Ok(cliente);
            }
        }
    }    
`

## 🚀 Começando

Essas instruções permitirão que você obtenha uma cópia do projeto em operação na sua máquina local para fins de desenvolvimento e teste!

### 📋 Pré-requisitos

De que coisas você precisa para instalar o software e como instalá-lo?

.NET 6: 
 - `https://dotnet.microsoft.com/en-us/download/dotnet/6.0`

SQL SERVER: 
 - `https://www.microsoft.com/pt-br/sql-server/sql-server-downloads`

Visual Studio 2022:

`https://visualstudio.microsoft.com/pt-br/vs/community/`
### 🔧 Instalação

Uma série de exemplos passo-a-passo que informam o que você deve executar para ter um ambiente de desenvolvimento em execução.

#### 1 - Clone o projeto:

`https://gitlab.com/haveros/gestao-clientes.git`

#### 2 - Abra sua IDE de desenvolvimento:

```
Aqui iremos utilizar o visualstudio 2022 para abrir o projeto.
```
#### 3 - configurando seu appsettings:

Abra o arquivo AppSettings.json e configure sua conexão com o banco de dados na chave `ConnectionStrings:DefaultConnection`


#### 3 - Execute as migrations

Defina o projeto GestaoCliente.Api como projeto de inicialização, abra npm console e execute o comando `Update-Database` para criar o banco de dados

##### Após isso, ao iniciar o projeto você será direcionado a página swagger onde poderá visualizar e testar os serviços do sistema.

## ⚙️ Padrões, modelagens e design patterns utilizados

- DDD 
- CQRS
- GOF
- Clean Clod
- SOLID

### ⌨️ Estilo de codificação e melhorias aplicadas

O projeto foi dividido em 5 camadas segregando responsábilidades para suas respectivas camadas:

 - GestaoCliente.Api
   - `Responsável por processar requisições e publicar eventos com CQRS
    `
- GestaoCliente.Application
   - `Responsável por processar os eventos, Contém os serviços, interfaces, dtos e regras de negócio da aplicação;
    `
- GestaoCliente.Domain
   - `Contém as entidades do domínio e as regras de negócio do projeto;
    `
- GestaoCliente.Domain.Core
   - `Responsável por tornar a camada de Domínio mais limpa, isola os objetos genéricos e padrões para todas as entidades;
    `
- GestaoCliente.Data
   - `Responsável por gerenciar o acesso a dados, contém as referências ao serviço de acesso a dados e os repositórios;
    `

## 📦 Resumo geral de melhorias

- Padronização de nomenclaturas e formatação de código.
- Remoção de lógica da controler, assim sua responsábilidade é somente comunicação entre camadas.
- Segregação de responsábilidades com CQRS e DDD definindo camadas, responsábilidade, e segregação de leitura e escrita no banco de dados.
- Padronização da API definindo melhor clareza nas Rotas, status de retorno e propriedades.

## ✒️ Autores

* **Mateus Primo Cardoso** - *trabalho completo* - [linkedin](https://www.linkedin.com/in/mateus-primo-cardoso-93a190136/)
